package com.example.lection4

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*
import android.util.Log.d as d

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        val generateRandomNumberButton = findViewById<Button>(R.id.generateRandomNumberButton)
        val randomNumberTextView = findViewById<Button>(R.id.randomnumberTextView)

        generateRandomNumberButton.setOnClickListener {
            val number: Int = randomNumber()
            d(attributionTag: "randomNumber", msg "This Is A Random Number")
            randomNumberTextView.text = number.toString()
        }
    }


    private fun randomNumber() = (-100..100).random()


        fun divideby5(): String {
            val randomNumber = randomNumber()
            if (randomNumber%5 == 0){
                return "Yes"
        }else{
                return "No"
            }

    }
}